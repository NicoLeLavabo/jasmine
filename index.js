function non(maVar) {
    return !maVar;
}
function plus(a, b) {
    return a + b;
}
function random(min, max) {
    let result = Math.round(Math.random() * (max - min) + min);
    return result;
}
//Test 1
describe("la function non", function () {
    var resultat;
    it("ca inverse vrai en faux", function () {
        resultat = true;
        expect(non(true)).toBe(false);
    });
});

//test 2
describe("la fonction plus addtionne", function () {
    it("ajouté 2 et 3 et renvoie 5", function () {
        expect(plus(2, 3)).toBe(5);
    });
    it("ajouté 4 et 9 et renvoie 13", function () {
        expect(plus(4, 9)).toBe(13);
    })
    it("ajouté 6 et 0 et renvoie 6", function () {
        expect(plus(6, 0)).toBe(6);
    })
})

//test 3 
describe("la function random", function () {
    it("génère un nombre aleatoire entre 0 et 10", function () {
        expect(random(0, 10) >= 0 && random(0, 10) <= 10).toBeTruthy();
    });
});